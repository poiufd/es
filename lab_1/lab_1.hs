{-# OPTIONS -fno-warn-tabs #-} 
import Data.List
import Debug.Trace

ant = ["abcde","fbcde","cdefg","ebcgh"]
cons = ['f','g','h','z']
rules = zip ant cons

forward fact goal temp
	| goal `elem` fact = True
	| fact == temp = False
	| otherwise = trace ( "search = " ++ show search) forward (search) goal fact where
		search = foldl (\stack (a,b)->  if length (filter (\el-> el `elem` fact) a) == length a
						&& b `notElem` stack then stack ++ [b] else stack ) fact rules 

backward fact goal temp
	| fact `elem` goal = True
	| goal == temp = False
	| otherwise = backward fact search_back goal where
		search_back = foldl (\stack (a,b)-> if b `elem` goal then nub $ stack ++ a else stack) goal rules


main = print $ forward "abcde" 'z' ""
--main = print $ backward 'a' "z" ""