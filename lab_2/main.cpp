#include <QCoreApplication>
#include "iostream"
#include <string>
#include <tuple>
#include <stdexcept>
#include <QFile>
#include <QTextStream>

using namespace std;

struct Fact
{
    string name;
    list<string> terms;
};

struct Rule
{
    Fact header;
    list<Fact> conditions;
};

Fact* facts;
Rule* rules;

void make_knowledge_base(string file_name, int *Nf, int *Nr)
{
    QFile file(QString::fromStdString(file_name));

        if((!file.open(QIODevice::ReadOnly | QIODevice::Text))) {
            cout << "error: " + file.errorString().toStdString();
        }

        QTextStream in(&file);
        in.setCodec("UTF-8");

        QString line = in.readLine();
        *Nf = line.toInt();
        facts = new Fact[*Nf];

        for (int i = 0; i < *Nf; i++)
        {
            QString line = in.readLine();
            QStringList param = line.split(" ");
            facts[i].name = param[0].toStdString();
            for (auto el:param)
                facts[i].terms.push_back(el.toStdString());
            facts[i].terms.pop_front();
        }
        line = in.readLine();
        *Nr = line.toInt();
        rules = new Rule[*Nr];

        Fact temp_fact;

        for (int i = 0; i < *Nr; i++)
        {
            QString line = in.readLine();
            QStringList p1 = line.split(",");
            QStringList p2 = p1[0].split(" ");
            rules[i].header.name = p2[0].toStdString();
            for (auto el:p2)
                rules[i].header.terms.push_back(el.toStdString());
            rules[i].header.terms.pop_front();

            for( int j = 1; j < p1.length(); j++)
            {
                p2 = p1[j].split(" ");

                temp_fact.name = p2[0].toStdString();;
                for (auto el:p2)
                    temp_fact.terms.push_back(el.toStdString());
                temp_fact.terms.pop_front();
                rules[i].conditions.push_back(temp_fact);
                temp_fact.terms.clear();
            }
        }
}

void print_fact(Fact f)
{
    cout << f.name + "(";
    for (auto iter = f.terms.begin(); iter != f.terms.end(); iter++)
    {
        cout << *iter  +" ";
    }
    cout << ")" << endl;
}

void print_rule(Rule r)
{
    print_fact(r.header);
    for (auto iter = r.conditions.begin(); iter != r.conditions.end(); iter++)
    {
        print_fact(*iter);
    }
}

bool check_goal(Fact goal, int Nf)
{
    for (int i = 0; i < Nf; i++)
    {
        if ((goal.name == facts[i].name) &&
            (goal.terms.size() == facts[i].terms.size()))
        {
            int count = 0;
            auto it1 = goal.terms.begin();
            auto it2 = facts[i].terms.begin();
            for(it1, it2; it1 != goal.terms.end(), it2 != facts[i].terms.end(); it1++, it2++)
            {
                if (*it1 == *it2)
                    count++;
            }
            if (count == goal.terms.size())
                return true;
        }
    }
    return false;
}

list<tuple<string,string>> replace_with_fact(Fact goal, int Nf)
{
    list<tuple<string,string>> result;

    for (int i = 0; i < Nf; i++)
    {
        if ((goal.name == facts[i].name) &&
            (goal.terms.size() == facts[i].terms.size()))
        {
            int count = 0;
            result.clear();
            auto it1 = goal.terms.begin();
            auto it2 = facts[i].terms.begin();
            for(it1, it2; it1 != goal.terms.end(), it2 != facts[i].terms.end(); it1++, it2++)
            {
                int pos1 = 0,pos2 = 0;
                //here check if string is constant
                if( (*it1).find_first_not_of("0123456789", pos1) == string::npos &&
                    (*it2).find_first_not_of("0123456789", pos2) == string::npos )
                {
                    if (*it1 == *it2)
                    {
                        count++;
                    }
                }
                pos1 = 0; pos2 = 0;

                if ((*it1).find_first_not_of("0123456789", pos1) != string::npos &&
                     (*it2).find_first_not_of("0123456789", pos2) == string::npos )
                {
                    count++;
                    result.push_back(make_tuple((*it1),(*it2)));
                }
            }
            if (count == goal.terms.size())
                return result;
        }
    }
    throw logic_error("");
}

list<tuple<string,string>> replace_with_rule(Fact goal, int i)
{
    list<tuple<string,string>> result;

    if ((goal.name == rules[i].header.name) &&
        (goal.terms.size() == rules[i].header.terms.size()))
    {
        int count = 0;
        result.clear();
        auto it1 = goal.terms.begin();
        auto it2 = rules[i].header.terms.begin();
        for(it1, it2; it1 != goal.terms.end(), it2 != rules[i].header.terms.end(); it1++, it2++)
        {
            int pos1 = 0,pos2 = 0;
            //here check if string is constant
            if( (*it1).find_first_not_of("0123456789", pos1) == string::npos &&
                (*it2).find_first_not_of("0123456789", pos2) == string::npos )
            {
                if (*it1 == *it2)
                {
                    count++;
                }
            }
            else
            {
                count++;
                result.push_back(make_tuple((*it2),(*it1)));
            }
         }

        if (count == goal.terms.size())
            return result;
    }
    throw logic_error("");
}

list<Fact> do_replace(int num, list<tuple<string,string>> l)
{
    list<Fact> result(rules[num].conditions);

    for (auto iter = result.begin(); iter != result.end(); iter++)
        for (auto elem : l)
            for (auto term = (*iter).terms.begin(); term != (*iter).terms.end(); term++)
            {
                if((*term) == get<0>(elem))
                    *term = get<1>(elem);
            }
    return result;
}

bool value = false;

bool solve(list<Fact> goals, int counter,int Nf,int Nr)
{
    //1.
    if (counter > 5)
        return false;
    if (value)
        return true;

    cout << "Goals: " << endl;
    for (auto el: goals)
        print_fact(el);

    for (auto goal = goals.begin(); goal != goals.end(); goal++)
    {
        if (check_goal(*goal,Nf))
        {
            cout << "Continue" << endl;
        }
        else
        {
            try
            {
                //2.
                list<tuple<string,string>> res = replace_with_fact(*goal,Nf);
                //debug
                for (auto v : res)
                    cout << get<0>(v) + "," + get<1>(v) << "\n";
                //unification
                for (auto g = goals.begin();g != goals.end(); g++)
                {
                    for ( auto it = (*g).terms.begin();it != (*g).terms.end(); it++)
                    {
                        for (auto l : res)
                        {
                            if (*it == get<0>(l))
                            {
                                *it = get<1>(l);
                            }
                        }
                    }
                }
                return solve(goals,counter,Nf,Nr);
            }
            catch (const logic_error& e)
            {
                //3.
                list<Fact> temp(goals);
                auto temp_pnt = goal;
                counter++;
                for (int i = 0; i < Nr; i++)
                {
                    goals = temp;
                    goal = temp_pnt;
                    try
                    {
                        list<tuple<string,string>> res = replace_with_rule(*goal,i);
                        list<Fact> new_goals = do_replace(i,res);

                        auto el = new_goals.begin();
                        *goal = *el;
                        ++el;
                        ++goal;
                        while(el != new_goals.end())
                        {
                            goals.insert(goal,*el);
                            ++goal;
                            ++el;
                        }
                        value += solve(goals,counter,Nf,Nr);
                    }
                    catch (logic_error& e)
                    {
                    }
                }
                return value;
            }
        }
    }
    return true;
}



int main(int argc, char *argv[])
{
    int Nf,Nr;
    make_knowledge_base("in.txt",&Nf,&Nr);

    for (int i = 0; i < Nf; i++)
        print_fact(facts[i]);
    for (int i = 0; i < Nr; i++)
        print_rule(rules[i]);

    Fact goal;
    goal.name = "f";
    goal.terms.push_back("1");
    goal.terms.push_back("6");
    list<Fact> goals;
    goals.push_back(goal);

    cout << solve(goals,0,Nf,Nr) << endl;
    return 0;
}
